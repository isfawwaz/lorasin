<?php
return [
	/*
    |--------------------------------------------------------------------------
    | Theme Textdomain
    |--------------------------------------------------------------------------
    |
    | Determines a textdomain for your theme. Should be used to dynamically set
    | namespace for gettext strings across theme. Remember, this value must
    | be in sync with `Text Domain:` entry inside style.css theme file.
    |
    */
   'textdomain' => 'lorasin',

    /*
    |--------------------------------------------------------------------------
    | Footer Text
    |--------------------------------------------------------------------------
    |
    | Change your footer text in the right screen.
    | It can be anything you want, like theme version or something else.
    |
    */
    'footer_text' => 'Lorasin.',

    /*
    |--------------------------------------------------------------------------
    | Post Format
    |--------------------------------------------------------------------------
    |
    | Change post format for post.
    |
    */
    'post_format' => [
        'gallery',
        'video'
    ],

    /*
    |--------------------------------------------------------------------------
    | Excerpt Length
    |--------------------------------------------------------------------------
    |
    | Test
    |
    */
    'excerpt_length' => 20,
];