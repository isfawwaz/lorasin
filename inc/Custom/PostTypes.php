<?php

namespace Lorasin\Custom;

/**
 * Custom
 * use it to write your custom functions.
 */
class PostTypes
{
	/**
     * register default hooks and actions for WordPress
     * @return
     */
	public function register() {
		add_action( 'init', array( $this, 'args_type'), 10 , 4 );
		add_action( 'after_switch_theme', array( $this, 'rewrite_flush') );	
	}	
	
  /**
    * Create Custom Post Types
    * @return The registered post type object, or an error object
    */
    public function args_type()
    {
		$posts = apply_filters( 'lorasin_custom_post_types', [] );

		if( !is_array($posts)) {
			$posts = [];
		}

		$counter = 1;

		foreach ($posts as $name => $args) {
	        $tags = !isset($args['taxonomies']) ? array() : $args['taxonomies'];

	        $labels = array(
	            'name'               => _x( $args['plural'], 'post type general name', $args['text_domain'] ),
				'singular_name'      => _x( $args['singular'], 'post type singular name', $args['text_domain'] ),
				'menu_name'          => _x( $args['plural'], 'admin menu', $args['text_domain'] ),
				'name_admin_bar'     => _x( $args['singular'], 'add new on admin bar', $args['text_domain'] ),
				'add_new'            => _x( 'Add New ' . $args['singular'], $args['text_domain'] ),
				'add_new_item'       => __( 'Add New ' . $args['singular'], $args['text_domain'] ),
				'new_item'           => __( 'New ' . $args['singular'], $args['text_domain'] ),
				'edit_item'          => __( 'Edit ' . $args['singular'], $args['text_domain'] ),
				'view_item'          => __( 'View ' . $args['singular'], $args['text_domain'] ),
				'view_items'         => __( 'View ' . $args['plural'], $args['text_domain'] ),
				'all_items'          => __( 'All ' . $args['plural'], $args['text_domain'] ),
				'search_items'       => __( 'Search' . $args['plural'], $args['text_domain'] ),
				'parent_item_colon'  => __( 'Parent ' . $args['plural'], $args['text_domain'] ),
				'not_found'          => __( 'No ' . $args['plural'] . ' found.', $args['text_domain'] ),
				'not_found_in_trash' => __( 'No ' . $args['plural'] . ' found in Trash.', $args['text_domain'] ),
			);
			
			do_action('lorasin_before_custom_post_registered');

	        register_post_type($name, array(
	            'labels'            => $labels,
	            'public'            => true,
	            'has_archive'        => true,

	            'publicly_queriable'    => true,
	            'show_ui'               => true,
	            'exclude_from_search'   => true,
	            'show_in_nav_menus'     => true,
	            'show_in_menu'            => true,

	            'capability_type'   => 'post',
	            'supports'          => array('page-attributes', 'title', 'thumbnail', 'category', 'editor', 'gallery'),
	            'rewrite'           => array('slug' => $args['slug']),
	            'menu_position'     => (7 + $counter),
	            'menu_icon'            => $args['menu_icon'],

	            'taxonomies' => $tags

			));
			
			do_action('lorasin_after_custom_post_registered');

	        $counter++;
	    }
	}

  /**
    * Flush Rewrite on CPT activation
    * @return empty
    */
    public function rewrite_flush()
    {
        // Flush the rewrite rules only on theme activation
        flush_rewrite_rules();
    }
}
