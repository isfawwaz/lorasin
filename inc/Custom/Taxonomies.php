<?php

namespace Lorasin\Custom;

class Taxonomies {
	/**
     * register default hooks and actions for WordPress
     * @return
     */
	public function register() {
		add_action( 'init', [ $this, 'hook_custom_taxonomies' ], 10 , 4 );
		add_action( 'after_switch_theme', array( $this, 'rewrite_flush') );	
	}

	/**
     * Create Custom Taxonomies
     * @return The registered taxonomies object, or an error object
     */
	public function hook_custom_taxonomies() {
		$taxonomies = apply_filters( 'lorasin_custom_taxonomies', [] );

		if( !is_array($taxonomies)) {
			$taxonomies = [];
		}

		$counter = 1;

		foreach( $taxonomies as $type => $args ) :
			$post = $args['post'];
            $h = isset($args['hierarchical']) ? $args['hierarchical'] : false;
            $p = isset($args['public']) ? $args['public'] : true;

            do_action('lorasin_before_taxonomy_registered');

			register_taxonomy($type, $post, [
                'public' => $p,
                'hierarchical'      => $h,
                'labels'             => [
                    'name'                       => $args['title'],
                    'singular_name'              => $args['title'],
                    'search_items'               => 'Cari ' . $args['title'],
                    'all_items'                  => 'Semua ' . $args['title'],
                    'parent_item'                => 'Kategori Induk',
                    'parent_item_colon'          => 'Kategori Induk:',
                    'edit_item'                  => 'Edit ' . $args['title'],
                    'update_item'                => 'Perbarui ' . $args['title'],
                    'add_new_item'               => 'Tambah ' . $args['title'],
                    'new_item_name'              => $args['title'] . ' Baru',
                    'not_found'                  => $args['title'] . ' Tidak Ditemukan',
                    'menu_name'                  => $args['title'],
                ],
                'show_ui'           => true,
                'show_admin_column' => true,
                'query_var'         => true,
                'rewrite'           => ['slug' => $args['slug']],
            ]);
            
            do_action('lorasin_after_taxonomy_registered');
		endforeach;
	}

	/**
     * Flush Rewrite on CPT activation
     * @return empty
     */
    public function rewrite_flush()
    {
        // Flush the rewrite rules only on theme activation
        flush_rewrite_rules();
    }
}