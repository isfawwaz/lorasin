<?php

namespace Lorasin\Core;

/**
 * Sidebar.
 */
class Sidebar
{
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register()
    {
        add_action( 'widgets_init', array( $this, 'widgets_init' ) );
    }

    /*
        Define the sidebar
    */
    public function widgets_init()
    {
        $widgets = [
            [
                'title' => __( 'Blog Sidebar', 'lorasin' ),
                'id' => 'blog-sidebar',
            ],
        ];

        $widgetsNoTitle = [
            [
                'title' => __( 'Footer One Sidebar', 'lorasin' ),
                'id' => 'footer-one-sidebar',
            ],
        ];

        // Apply filters
        $widgets = apply_filters( "lorasin_widgets", $widgets );
        $widgetsNoTitle = apply_filters( "lorasin_widgets_no_title", $widgetsNoTitle );

        // Register sidebar normal
        if( is_array($widgets) ) {
            foreach( $widgets as $widget ) {
                $this->register_sidebar( $widget );
            }
        } else {
            $this->register_sidebar( $widgets );
        }

        // Register sidebar with no title
        if( is_array($widgetsNoTitle) ) {
            foreach( $widgetsNoTitle as $widget ) {
                $this->register_sidebar( $widget, false );
            }
        } else {
            $this->register_sidebar( $widgetsNoTitle, false );
        }
    }

    private function register_sidebar( $args, $withTitle = true ) {
        $beforeWidget = apply_filters( 'lorasin_widget_before_widget', '<section id="%1$s" class="widget %2$s p-2">' );
        $afterWidget = apply_filters( 'lorasin_widget_after_widget', '</section>' );
        if( $withTitle ) {
            $beforeTitle = apply_filters( 'lorasin_widget_before_title', '<h2 class="widget-title">' );
            $afterTitle = apply_filters( 'lorasin_widget_after_title', '</h2>' );
        } else {
            $beforeTitle = apply_filters( 'lorasin_widget_before_title_hidden', '<h2 class="widget-title" style="display: none;">' );
            $afterTitle = apply_filters( 'lorasin_widget_after_title_hidden', '</h2>' );
        }

        register_sidebar( array(
            'name' => esc_html__( $args['title'], 'lorasin'),
            'id' => $args['id'],
            'description' => esc_html__('Sidebar to add all your widgets.', 'lorasin'),
            'before_widget' => $beforeWidget,
            'after_widget' => $afterWidget,
            'before_title' => $beforeTitle,
            'after_title' => $afterTitle,
        ) );
    }
}
