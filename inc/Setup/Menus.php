<?php

namespace Lorasin\Setup;

/**
 * Menus
 */
class Menus
{
    private $listMenu;
    /**
     * register default hooks and actions for WordPress
     * @return
     */
    public function register()
    {
        // Load variables config
        $this->listMenu = require_once dirname( __FILE__ ) . '/../../config/menus.php';

        // Start action
        add_action( 'after_setup_theme', array( $this, 'menus' ) );
    }

    public function menus()
    {
        /*
            Register all your menus here
        */
       $menus = apply_filters( 'lorasin_menus', $this->listMenu );
        register_nav_menus( $menus );
    }
}
