<?php
/**
 * Callbacks for Settings API
 *
 * @package awps
 */

namespace Lorasin\Api\Callbacks;

/**
 * Settings API Callbacks Class
 */
class SettingsCallback
{
	public function admin_index() 
	{
		return require_once( get_template_directory() . '/views/admin/index.php' );
	}

	public function admin_company_information() {
		return require_once( get_template_directory() . '/views/admin/company.php' );
	}

	public function admin_social_media() {
		return require_once( get_template_directory() . '/views/admin/sosmed.php' );
	}

	public function admin_custom_css() {
		return require_once( get_template_directory() . '/views/admin/custom-css.php' );
	}

	public function admin_custom_js() {
		return require_once( get_template_directory() . '/views/admin/custom-js.php' );
	}

	public function admin_faq() 
	{
		echo '<div class="wrap"><h1>FAQ Page</h1></div>';
	}

	public function awps_options_group( $input ) 
	{
		return $input;
	}

	public function awps_admin_index() 
	{
		echo 'Customize this Theme Settings section and add description and instructions';
	}

	public function theme_setting_company_information() {
		echo 'Customize your company information';
	}

	public function theme_setting_sosmed() {
		echo 'Customize your company social media information';	
	}

	public function theme_setting_custom_css() {
		echo 'Add custom css to your site';
	}

	public function theme_setting_custom_js() {
		echo 'Add custom javascript to your site';
	}

	public function first_name()
	{
		$first_name = esc_attr( get_option( 'first_name' ) );
		echo '<input type="text" class="regular-text" name="first_name" value="'.$first_name.'" placeholder="First Name" />';
	}

	public function field_render( $input ) {
		$data = null;
		if( isset($input['option']) ) {
			$data = esc_attr( get_option( $input['option'], null ) );
		}

		echo '<input type="text" class="regular-text" name="'. $input['name'] .'" value="'.$data.'" placeholder="'. $input['placeholder'].'" />';
		if( isset($input['description']) ) {
			echo '<p class="description" id="'. $input['name'] .'-description">'. $input['description'] .'</</p>';
		}
	}

	public function field_render_textarea( $input ) {
		$data = null;
		if( isset($input['option']) ) {
			$data = esc_attr( get_option( $input['option'], null ) );
		}

		$rows = 10;
		$cols = 50;
		if( isset($input['rows']) && !empty($input['rows']) && is_numeric($input['rows']) ) {
			$rows = $input['rows'];
		}
		if( isset($input['cols']) && !empty($input['cols']) && is_numeric($input['cols']) ) {
			$cols = $input['cols'];
		}

		if( isset($input['description']) ) {
			echo '<p id="'. $input['name'] .'-description">'. $input['description'] .'</</p>';
		}
		echo '<p><textarea name="'. $input['name'] .'" rows="'. $rows .'" cols="'. $cols .'" class="large-text code" spellcheck="false" placeholder="'. $input['placeholder'] .'">'. $data .'</textarea></p>';
	}

	public function field_code_editor( $input ) {
		$data = null;
		if( isset($input['option']) ) {
			$data = wp_unslash( get_option( $input['option'], null ) );
		}

		if( isset($input['description']) ) {
			echo '<p id="'. $input['name'] .'-description">'. $input['description'] .'</</p>';
		}
         echo '<p><textarea id="'. $input['id'] .'" rows="5" name="'. $input['name'] .'" class="widefat textarea">'. $data .'</textarea></p>';
	}
}