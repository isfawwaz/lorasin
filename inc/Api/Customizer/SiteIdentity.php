<?php
/**
 * Theme Customizer - Site Identity
 *
 * @package lorasin
 */

namespace Lorasin\Api\Customizer;

use WP_Customize_Control;
use WP_Customize_Color_Control;
use WP_Customize_Image_Control;

use Lorasin\Api\Customizer;

/**
 * Customizer class
 */
class SiteIdentity {
	/**
	 * register default hooks and actions for WordPress
	 * @return
	 */
	public function register( $wp_customize ) {
		$wp_customize->add_setting( 'lorasin_logo_invert', [
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		]);

		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'lorasin_logo_invert',
			[
				'label' => __('Logo Invert', 'origamicrane'),
				'section' => 'title_tagline',
				'settings' => 'lorasin_logo_invert',
				'priority' => 8,
				'button_labels' => [
					'select' => __( 'Select logo invert', 'lorasin' ),
					'change' => __( 'Change logo invert', 'lorasin' ),
					'remove' => __( 'Remove logo invert', 'lorasin' ),
				]
			]
		)
	);
	}
}