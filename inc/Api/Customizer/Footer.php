<?php
/**
 * Theme Customizer - Footer
 *
 * @package lorasin
 */

namespace Lorasin\Api\Customizer;

use WP_Customize_Control;
use WP_Customize_Color_Control;

use Lorasin\Api\Customizer;

/**
 * Customizer class
 */
class Footer 
{
	/**
	 * register default hooks and actions for WordPress
	 * @return
	 */
	public function register( $wp_customize ) 
	{
		$wp_customize->add_section( 'lorasin_footer_section' , array(
			'title' => __( 'Footer', 'lorasin' ),
			'description' => __( 'Customize the Footer' ),
			'priority' => 30
		) ); 

		$wp_customize->add_setting( 'lorasin_footer_text' , array(
			'default' => 'About this company',
			'transport' => 'postMessage', // or refresh if you want the entire page to reload
		) );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'lorasin_footer_text', array(
			'label' => __( 'About Footer Text', 'lorasin' ),
			'section' => 'lorasin_footer_section',
			'settings' => 'lorasin_footer_text',
			'type' => 'textarea',
		) ) );

		if ( isset( $wp_customize->selective_refresh ) ) {
			$wp_customize->selective_refresh->add_partial( 'lorasin_footer_text', array(
				'selector' => '#lorasin-footer-copy-control',
				'render_callback' => array( $this, 'outputText' ),
				'fallback_refresh' => true
			) );
		}
	}

	/**
	 * Generate inline CSS for customizer async reload
	 */
	public function outputCss()
	{
		echo '<style type="text/css">';
			echo Customizer::css( '.site-footer', 'background-color', 'lorasin_footer_background_color' );
		echo '</style>';
	}

	/**
	 * Generate inline text for customizer async reload
	 */
	public function outputText()
	{
		echo Customizer::text( 'lorasin_footer_text' );
	}
}