<?php
/**
 * Template part for displaying a custom Admin area
 *
 * @link https://developer.wordpress.org/reference/functions/add_menu_page/
 *
 * @package lorasin
 */

?>

<div class="wrap">
	<h1>Theme Settings</h1>
	<?php settings_errors(); ?>

	<form method="post" action="options.php">
		<?php settings_fields( 'lorasin_options_js_group' ); ?>
		<?php do_settings_sections( 'theme_setting_custom_js' ); ?>
		<?php submit_button(); ?>
	</form>
</div>
