<?php
/**
 * Template part for displaying a custom Admin area
 *
 * @link https://developer.wordpress.org/reference/functions/add_menu_page/
 *
 * @package lorasin
 */

?>

<div class="wrap">
	<h1>Theme Settings - Company Information</h1>
	<?php settings_errors(); ?>

	<form method="post" action="options.php">
		<?php settings_fields( 'lorasin_option_group' ); ?>
		<?php do_settings_sections( 'theme_settings' ); ?>
		<?php submit_button(); ?>
	</form>
</div>
